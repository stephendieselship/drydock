const db = require("../models");
const translations = db.translations;
const countries = db.countries;
const currency = db.currency;
const Op = db.Sequelize.Op;
const Validate = require("express-validation");
// Create and Save a new roles


 
// Retrieve all roless from the database.
exports.languageAll = (req, res) => {


  translations.findAll()
    .then(data => {


      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving languages."
      });
    });
};

exports.countriesAll = (req, res) => {


  countries.findAll()
  .then(data => {


      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving countries."
      });
    });
};


exports.currencyAll = (req, res) => {


  currency.findAll()
    .then(data => {


      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving currencies."
      });
    });
};

