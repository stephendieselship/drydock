const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const Role = db.role;
const User_roles = db.user_roles;
const Op = db.Sequelize.Op;
const fs = require('fs');


var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const path = require("path");

// function decodeBase64Image(dataString) {
//   var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
//     response = {};

//   if (matches.length !== 3) {
//     return new Error('Invalid input string');
//   }

//   response.type = matches[1];
//   response.data = new Buffer(matches[2], 'base64');

//   return response;
// }



// Update a Tutorial by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;
  User.findOne({
   
    where: {
       id: { [Op.ne]: id },
      [Op.or]: [{
        email: req.body.email},
       { username: req.body.username },
        {user_id: req.body.user_id },
        {emp_id: req.body.emp_id },
      ]
    }
  }).then(function(user) {

    if (user){
    if (user.user_id==req.body.user_id) {
      res.status(500).send({ message: 'user_id' });
    } else if (user.email==req.body.email) {
      res.status(500).send({ message: 'email' });
    } else if (user.emp_id==req.body.emp_id) {
      res.status(500).send({ message: 'emp_id' });
    } else if (user.username==req.body.username) {
      res.status(500).send({ message: 'username' });
    } else {
      res.status(500).send({ message: 'Some error occurred while saving Users.' });
    } 
      
  }else{

if(req.body.difavatar==1){
      // to declare some path to store your converted image

     
     
      const imgdata = req.body.avatar;
      req.body.avatar=Date.now()+req.body.username+'.jpg';

      const path = "./images/avatar/"+ req.body.avatar
      // to convert base64 format into random filename
      const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
      fs.writeFile(path, base64Data,{encoding: 'base64'}, function (err) {
        res.send({ message: 'Some error occurred while saving User image.' });
      });
      // fs.writeFileSync(path, base64Data,  {encoding: 'base64'});

     
    }
     


   let data = {username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
      username: req.body.username,
      emp_id: req.body.emp_id,
      user_id: req.body.user_id,
      avatar: req.body.avatar,
      phone: req.body.phone,
      role: req.body.role,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      country: req.body.country,
      language: req.body.language,
      is_super_admin: req.body.is_super_admin,
      address: req.body.address,
      superintendent_id: req.body.superintendent_id}
  User.update(data,{
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating User with id=" + id
      });
    });
  }  });
};
exports.create = (req, res) => {
  // Save User to Database
  User.findOne({
    where: {
      [Op.or]: [{
        email: req.body.email},
       { username: req.body.username },
        {user_id: req.body.user_id },
        {emp_id: req.body.emp_id },
      ]
    }
  }).then(function(user) {

    if (user){
    if (user.user_id==req.body.user_id) {
      res.status(500).send({ message: 'user_id' });
    } else if (user.email==req.body.email) {
      res.status(500).send({ message: 'email' });
    } else if (user.emp_id==req.body.emp_id) {
      res.status(500).send({ message: 'emp_id' });
    } else if (user.username==req.body.username) {
      res.status(500).send({ message: 'username' });
    } else {
      res.status(500).send({ message: 'Some error occurred while saving Users.' });
    } 
      
  }else{
  if(req.body.avatar){

  
      // to declare some path to store your converted image
      const imgdata = req.body.avatar;
      req.body.avatar=Date.now()+req.body.username+'.jpg';

      const path = "./images/avatar/"+ req.body.avatar
      // to convert base64 format into random filename
      const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
      fs.writeFile(path, base64Data,{encoding: 'base64'}, function (err) {
        res.send({ message: 'Some error occurred while saving User image.' });
      });
      // fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
    }else
    req.body.avatar="";

 

  User.create({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    username: req.body.username,
    emp_id: req.body.emp_id,
    user_id: req.body.user_id,
    avatar: req.body.avatar,
    phone: req.body.phone,
    role: req.body.role,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    country: req.body.country,
    language: req.body.language,
    is_super_admin: req.body.is_super_admin,
    address: req.body.address,
    superintendent_id: req.body.superintendent_id
  })
    .then(user => {
     
      if (req.body.role) {
       
        Role.findByPk(req.body.role).then(roles => {

        
         
          user.setRoles(roles).then(() => {
            res.send({ message: "User registered successfully!" });
          });
        });
      } else {
        // user role = 1
        user.setRoles([1]).then(() => {
          res.send({ message: "User registered successfully!" });
        });
      }
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
  }
});
};




exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
  res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

exports.ManagerBoard = (req, res) => {
  res.status(200).send("Manager Content.");
};

exports.getAll = (req, res) => {


  User.findAll()
    .then(data => {


      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Users."
      });
    });
};

exports.updatestatus = (req, res) => {
  const id = req.params.id;

  User.update({
    status: req.body.status}, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating User with id=" + id
      });
    });
};


exports.getByid = (req, res) => {
  const id = req.params.id;

  User.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id
      });
    });
};
exports.findAll = (req, res) => {
  var name = req.body?.search?.value;
  var condition = name ? {
    [Op.or]: [{
      firstname: { [Op.like]: `%${name}%` }},
     { lastname: { [Op.like]: `%${name}%` }},
      {username: { [Op.like]: `%${name}%` }},
      {email: { [Op.like]: `%${name}%` }},
    ]
  } : null;
let recordstotal;
let recordstotal2;
  User.findAll()
  .then(data => {
    
    recordstotal =data.length;
    if(name)
    recordstotal2 =0;
    else
    recordstotal2 =data.length;
  })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Users."
      });
    });
  User.findAll({ where: condition,offset: req.body.start, limit: req.body.length ,
  
    include: [
      {
        model: User_roles, 
        
      }
    ],
    include: [{
      model: Role}
    ]  
  })
    .then(data => {
      let data1 ;
      if(recordstotal2==0){
         data1 = {
          draw: req.body.draw,
          recordsTotal: recordstotal,
          recordsFiltered: data.length,
          data: data
        };
      }else{
         data1 = {
          draw: req.body.draw,
          recordsTotal: recordstotal,
          recordsFiltered: recordstotal,
          data: data
        };
      }
     
      res.send(data1);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Users."
      });
    });
};

// Delete a roles with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  User.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete User with id=" + id
      });
    });
};
