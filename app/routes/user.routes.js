const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/all", controller.allAccess);
  app.post(
    "/api/all-users",
    [authJwt.verifyToken],
    controller.findAll
  );
  app.get(
    "/api/users",
    [authJwt.verifyToken],
    controller.getAll
  );
  app.put(
    "/api/users/:id",
    [authJwt.verifyToken],
    controller.update
  );
  app.put(
    "/api/users/status-change/:id",
    [authJwt.verifyToken],
    controller.updatestatus
  );
  app.get(
    "/api/users/:id",
    [authJwt.verifyToken],
    controller.getByid
  );
  app.post(
    "/api/users",
    [authJwt.verifyToken],
    controller.create
  );
  app.delete(
    "/api/users/:id",
    [authJwt.verifyToken],
    controller.delete
  );
  app.get(
    "/api/test/user",
    [authJwt.verifyToken],
    controller.userBoard
  );

  app.get(
    "/api/test/mod",
    [authJwt.verifyToken, authJwt.isManager],
    controller.ManagerBoard
  );

  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );
};
