const controller = require("../controllers/tutorial.controller");
const { authJwt } = require("../middleware");


module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post(
    "/api/user/",
    [authJwt.verifyToken],
    controller.create
  );
  app.get(
    "/api/user/",
    [authJwt.verifyToken],
    controller.findAll
  );

  app.get(
    "/api/user/published",
    [authJwt.verifyToken],
    controller.findAllPublished
  );
  app.get(
    "/api/user/:id",
    [authJwt.verifyToken],
    controller.findOne
  );
  app.put(
    "/api/user/:id",
    [authJwt.verifyToken],
    controller.update
  );
  app.delete(
    "/api/user/:id",
    [authJwt.verifyToken],
    controller.delete
  );
  app.delete(
    "/api/user/",
    [authJwt.verifyToken],
    controller.deleteAll
  );
};

