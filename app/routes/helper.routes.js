const controller = require("../controllers/helper.controller");
const { authJwt } = require("../middleware");


module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/api/languages/",
    [authJwt.verifyToken],
    controller.languageAll
  );
  app.get(
    "/api/countries/",
    [authJwt.verifyToken],
    controller.countriesAll
  );
  app.get(
    "/api/currencies/",
    [authJwt.verifyToken],
    controller.currencyAll
  );
};

