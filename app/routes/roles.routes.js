const controller = require("../controllers/roles.controller");
const { authJwt } = require("../middleware");


module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post(
    "/api/roles/",
    [authJwt.verifyToken],
    controller.create
  );
  app.post(
    "/api/all-roles",
    [authJwt.verifyToken],
    controller.getAll
  );
  app.get(
    "/api/roles/",
    [authJwt.verifyToken],
    controller.findAll
  );
  app.get(
    "/api/roles/menus",
    [authJwt.verifyToken],
    controller.menufindAll
  );
  app.get(
    "/api/roles/allmenus",
    [authJwt.verifyToken],
    controller.menuAll
  );
  app.get(
    "/api/roles/published",
    [authJwt.verifyToken],
    controller.findAllPublished
  );
  app.get(
    "/api/permissions/:id",
    [authJwt.verifyToken],
    controller.role_details
  );
  app.get(
    "/api/roles/:id",
    [authJwt.verifyToken],
    controller.findOne
  );
  app.put(
    "/api/roles/:id",
    [authJwt.verifyToken],
    controller.update
  );
  app.put(
    "/api/permissions/:id",
    [authJwt.verifyToken],
    controller.menusupdate
  );
  app.delete(
    "/api/roles/:id",
    [authJwt.verifyToken],
    controller.delete
  );
  app.delete(
    "/api/roles/",
    [authJwt.verifyToken],
    controller.deleteAll
  );
};

