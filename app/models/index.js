const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: false,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.language = require("../models/language.model.js")(sequelize, Sequelize);
db.currency = require("../models/currency.model.js")(sequelize, Sequelize);
db.countries = require("../models/countries.model.js")(sequelize, Sequelize);
db.translations = require("../models/translations.model.js")(sequelize, Sequelize);
db.user_roles = require("../models/user_roles.model.js")(sequelize, Sequelize);
db.role_menu = require("../models/role_menu.model.js")(sequelize, Sequelize);
db.menu = require("../models/menu.model.js")(sequelize, Sequelize);
db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "roleid",
  otherKey: "userid"
});
db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userid",
  otherKey: "roleid"
});
db.role.belongsToMany(db.menu, {
  through: "role_menus",
  foreignKey: "role_id",
  otherKey: "menu_id"
});
db.menu.belongsToMany(db.role, {
  through: "role_menus",
  foreignKey: "menu_id",
  otherKey: "role_id"
});

// db.menu.belongsTo(db.role, {
//   as: 'roles', 
//   foreignKey: 'roleid',
// });

// db.role.belongsToMany(db.role_menu, {
//   as: 'roles',
//   foreignKey: "roleid"
 
// });
 

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;
