module.exports = (sequelize, Sequelize) => {
  const Role = sequelize.define("roles", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING
    },
    slug: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    }
  });
  // Role.associate = function (models) {
  //   Role.belongsTo(models.Role_menu)
  // }
  return Role;
};
