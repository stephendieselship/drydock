module.exports = (sequelize, Sequelize) => {
  const Translations = sequelize.define("translations", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING
    },
    language: {
      type: Sequelize.INTEGER
    },
    icon: {
      type: Sequelize.STRING
    },

    status: {
      type: Sequelize.INTEGER
    },
    deleted_at: {
      type: Sequelize.STRING
    }
  });

  return Translations;
};
