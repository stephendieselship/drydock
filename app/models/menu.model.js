module.exports = (sequelize, Sequelize) => {
  const Menu = sequelize.define("menus", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    parent_menu_id: {
      type: Sequelize.INTEGER
    },
    module: {
      type: Sequelize.STRING
    },
    label: {
      type: Sequelize.STRING
    },
    text: {
      type: Sequelize.STRING
    },
    link: {
      type: Sequelize.STRING
    },
    icon: {
      type: Sequelize.STRING
    },
    order: {
      type: Sequelize.INTEGER
    },
    subscription: {
      type: Sequelize.INTEGER
    },
    status: {
      type: Sequelize.INTEGER
    }
  });

  return Menu;
};
