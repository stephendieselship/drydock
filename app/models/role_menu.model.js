module.exports = (sequelize, Sequelize) => {
  const Role_menu = sequelize.define("role_menus", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    // roleid: {
    //   type: Sequelize.INTEGER
    // },
    // menuid: {
    //   type: Sequelize.INTEGER
    // },
    view: {
      type: Sequelize.INTEGER
    },
    created: {
      type: Sequelize.INTEGER
    },
    edited: {
      type: Sequelize.INTEGER
    },
    deleted: {
      type: Sequelize.INTEGER
    }
   
  });
  // Role_menu.associate = function (models) {
  //   Role_menu.hasMany(models.Role)
  // }
  return Role_menu;
};
