module.exports = (sequelize, Sequelize) => {
  const Countries = sequelize.define("countries", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING
    },
    code: {
      type: Sequelize.STRING
    },
   
  });

  return Countries;
};
