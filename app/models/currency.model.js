module.exports = (sequelize, Sequelize) => {
  const Currency = sequelize.define("currency", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING
    },
    code: {
      type: Sequelize.STRING
    },
    symbol: {
      type: Sequelize.STRING
    },
   
  });

  return Currency;
};
